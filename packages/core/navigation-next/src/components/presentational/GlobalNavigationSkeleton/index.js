// @flow

import { withGlobalTheme } from '../../../theme';
import GlobalNavigationSkeleton from './GlobalNavigationSkeleton';

export default withGlobalTheme(GlobalNavigationSkeleton);
