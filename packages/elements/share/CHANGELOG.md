# @atlaskit/share

## 0.1.1
- [patch] [64bf358](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/64bf358):

  - FS-3416 add ShareForm component to @atlaskit/share

## 0.1.0
- [minor] [891e116](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/891e116):

  - FS-3291 add share skeleton
