import React from 'react';

export default ({ onClick }) => <button onClick={onClick}>Manage</button>;
